@component('mail::message')
    # Pendaftaran Siswa

    Selamat, anda telah terdaftar di PPI 23 Cirengit Cangkuang Kabupaten Bandung.

    @component('mail::button', ['url' => 'https://github.com/hendisantika'])
        Klik Di sini
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
