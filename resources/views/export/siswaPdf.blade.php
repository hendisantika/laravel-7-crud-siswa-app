<table class="table">
    <thead>
    <tr>
        <th>NAMA LENGKAP</th>
        <th>JENIS KELAMIN</th>
        <th>AGAMA</th>
        <th>NILAI RATA2</th>
    </tr>
    </thead>
    <tbody>
    @foreach($siswa as $s)
        <tr>
            <td>{{$s -> namaLengkap()}}</td>
            <td>{{$s -> agama}}</td>
            <td>{{$s -> jenis_kelamin}}</td>
            <td>{{$s -> nilaiRata2()}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
