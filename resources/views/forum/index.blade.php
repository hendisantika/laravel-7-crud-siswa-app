@extends('layouts.master')

@section('content')
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Forum</h3>
                                <div class="right">
                                    <a href="{{route('posts.add')}}" class="btn btn-sm btn-primary">Tambah Komentar</a>
                                </div>

                            </div>
                            <div class="panel-body">
                                <div class="panel panel-scrolling">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Komentar terkini</h3>
                                        <div class="right">
                                            <button type="button" class="btn-toggle-collapse"><i
                                                    class="lnr lnr-chevron-up"></i></button>
                                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="slimScrollDiv"
                                         style="position: relative; overflow: hidden; width: auto; height: 430px;">
                                        <div class="panel-body" style="overflow: hidden; width: auto; height: 430px;">
                                            <ul class="list-unstyled activity-list">
                                                @foreach ($forum as $frm)
                                                    <li>
                                                        <img src=
                                                             "{{$frm->user->siswa->getAvatar()}}
                                                                 "
                                                             alt="Avatar" class="img-circle pull-left avatar">
                                                        <p>
                                                            <a href="#">{{$frm->user->siswa->nama_lengkap()}}</a> {{$frm->judul}}
                                                            <span
                                                                class="timestamp">{{$frm->created_at->diffForHumans()}}</span>
                                                        </p>
                                                    </li>
                                                @endforeach

                                            </ul>
                                            <button type="button" class="btn btn-primary btn-bottom center-block">Muat
                                                Lebih Banyak
                                            </button>
                                        </div>
                                        <div class="slimScrollBar"
                                             style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 292.563px;"></div>
                                        <div class="slimScrollRail"
                                             style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
