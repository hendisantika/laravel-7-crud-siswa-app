<?php
return [
    'logo_url' => '/frontend/img/logo.png',
    'image_banner_url' => '/images/landscape.jpg',
    'image_register_url' => '/images/registration.jpg',
    'title' => 'PPI 23 Cirengit',
    'welcome_message' => 'Selamat Datang di situs resmi PPI 23 Cirengit',
    'sub_welcome_message' => 'Sekolah dengan kurikulum terpadu dan inklusif',
    'welcome_message_button_text' => 'Jelajahi',
    'welcome_message_button_url' => '/Jelajahi',

    'telpon' => '022-86815190',
    'email' => 'ppi23_cirengit@gmail.com',

    'facebook_url' => 'https://www.facebook.com/hendisantika',
    'instagram_url' => 'https://www.instagram.com/hendisantika',

    'home_features_column_1_title' => 'Guru dan pengajar berpengalaman',
    'home_features_column_1_content' => 'Staf pengajar yang berdedikasi tinggi dengan kapasitas yang mumpuni ',
    'home_features_column_1_link_teks' => 'Lihat Guru',
    'home_features_column_1_link_url' => '/guru',

    'home_features_column_2_title' => 'Kurikulum terpadu',
    'home_features_column_2_content' => 'Menggabungkan antara kurikulum pemerintah dan kurikulum pesantren',
    'home_features_column_2_link_teks' => 'Lihat kurikulum',
    'home_features_column_2_link_url' => '/kurikulum',

    'home_features_column_3_title' => 'Berwawasan inklusif',
    'home_features_column_3_content' => 'Menciptakan iklim belajar yang inklusif dan ramah anak ',
    'home_features_column_3_link_teks' => 'Daftar',
    'home_features_column_3_link_url' => '',

];
