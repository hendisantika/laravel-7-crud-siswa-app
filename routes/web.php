<?php

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/kirimEmail', function () {
    Mail::raw('Halo Siswa Baru', function ($message) {
        $message->to('hendisantika@yahoo.co.id', 'Hendi Santika');
        $message->subject('Pendaftaran Siswa');
    });
});


Route::get('/', 'SiteController@home');
Route::get('/register', 'SiteController@register');
Route::post('/register', 'SiteController@daftar');
Route::get('/about', 'SiteController@about');

Route::get('/login', 'AuthController@formLogin')->name('login');
Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');

Route::group(['middleware' => ['auth', 'CheckRole:admin']], function () {
    Route::get('/siswa', 'SiswaController@index');
    Route::post('/siswa/create', "SiswaController@create");
    Route::get('/siswa/{siswa}/edit', 'SiswaController@edit');
    Route::post('/siswa/{siswa}/update', 'SiswaController@update');
    Route::get('/siswa/{siswa}/delete', 'SiswaController@delete');
    Route::get('/siswa/{siswa}/profile', 'SiswaController@profile');
    Route::post('/siswa/{siswa}/addnilai', 'SiswaController@addnilai');
    Route::get('/siswa/{siswa}/{idmapel}/deletenilai', 'SiswaController@deletenilai');
    Route::get('/siswa/exportExcel', 'SiswaController@exportExcel');
    Route::get('/siswa/exportPdf', 'SiswaController@exportPdf');
    Route::post('/siswa/import', 'SiswaController@importexcel')->name('siswa.import');
    Route::get('/guru/{id}/profile', 'GuruController@profile');
    Route::get('/posts', 'PostController@index')->name('posts.index');
    Route::get('/posts/add', [
        'uses' => 'PostController@add',
        'as' => 'posts.add'
    ]);

    Route::post('/posts/create', [
        'uses' => 'PostController@create',
        'as' => 'posts.create',
    ]);

});

Route::group(['middleware' => ['auth', 'CheckRole:admin,siswa']], function () {
    Route::get('/dashboard', "DashboardController@index");
    Route::get('/forum', 'ForumController@index');
});

Route::group(['middleware' => ['auth', 'CheckRole:siswa']], function () {
    Route::get('/profilsaya', 'SiswaController@profilsaya');

});

Route::get('getdatasiswa', [
    'uses' => 'SiswaController@getdatasiswa',
    'as' => 'ajax.get.data.siswa',
]);

Route::get('/{slug}', [
    'uses' => 'SiteController@singlepost',
    'as' => 'site.single.post'

]);
