<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Guru extends Model
{
    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Uuid::uuid4()->getHex());
        });
    }

    protected $table = 'guru';

    protected $fillable = ['nama', 'telpon', 'alamat'];

    public function mapel()
    {
        return $this->hasMany(Mapel::class);
    }


}
