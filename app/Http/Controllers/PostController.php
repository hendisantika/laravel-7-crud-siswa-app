<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('posts.index', compact(['posts']));
    }

    public function add()
    {
        return view('posts.add');
    }

    public function create(Request $request)
    {
        $posts = Post::create([

            'title' => $request->title,
            'content' => $request->content,
            'user_id' => auth()->user()->id,
            'thumbnail' => $request->thumbnail
        ]);
//        return Redirect::to('posts')->with('sukses', 'Post berhasil disubmit');
        return redirect()->route('posts.index')->with('sukses', 'Post berhasil disubmit');
    }
}
