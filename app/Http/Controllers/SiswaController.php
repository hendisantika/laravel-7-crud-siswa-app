<?php

namespace App\Http\Controllers;

use App\Exports\SiswaExport;
use App\Imports\SiswaImport;
use App\Mapel;
use App\Siswa;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class SiswaController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('cari')) {
            $data_siswa = Siswa::where('nama_depan', 'LIKE', '%' . $request->cari . '%')->paginate(20);
        } else {
            $data_siswa = Siswa::all();
        }
        return view('siswa.index', ['data_siswa' => $data_siswa]);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'nama_depan' => 'required|min:3',
            'nama_belakang' => 'required',
            'email' => 'required|email|unique:users',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'avatar' => 'mimes:jpg,png,jpeg'
        ]);

        //insert ke tabel user
        $user = new User;
        $user->role = 'siswa';
        $user->name = $request->nama_depan;
        $user->email = $request->email;
        $user->password = bcrypt('rahasia');
        $user->remember_token = Str::random(60);
        $user->save();

        //insert ke tabel siswa
        $request->request->add(['user_id' => $user->id]);
        $siswa = Siswa::create($request->all());
        if ($request->hasFile('avatar')) {
            $request->file('avatar')->move('images', $request->file('avatar')->getClientOriginalName());
            $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            $siswa->save();
        }
        return redirect('/siswa')->with('sukses', 'Data Berhasil Diinput');
    }

    public function edit(Siswa $siswa)
    {
        return view('siswa/edit', ['siswa' => $siswa]);
    }

    public function update(Request $request, Siswa $siswa)
    {
        $siswa->update($request->all());
        if ($request->hasFile('avatar')) {
            $tujuan_upload = 'images';
            $request->file('avatar')->move($tujuan_upload, $request->file('avatar')->getClientOriginalName());
            $siswa->avatar = $request->file('avatar')->getClientOriginalName();
            $siswa->save();
        }

        return redirect('/siswa')->with('sukses', 'Data Berhasil diupdate');
    }

    public function delete(Siswa $siswa)
    {
        $siswa->delete();
        Session::flash('sukses', 'Ini notifikasi SUKSES');
        return redirect('/siswa')->with('sukses', 'Data Berhasil dihapus');
    }

    public function profile(Siswa $siswa)
    {
        $mapel = Mapel::all();

//        Menyiapkan data untuk chart
        $categories = [];
        $data = [];
        foreach ($mapel as $mp) {
            if ($siswa->mapel()->wherePivot('mapel_id', $mp->id)->first()) {
                $categories[] = $mp->nama;
                $data[] = $siswa->mapel()->wherePivot('mapel_id', $mp->id)->first()->pivot->nilai;
            }
        }
//        dd($data);
        return view('siswa.profile', ['siswa' => $siswa, 'mapel' => $mapel, 'categories' => $categories, 'data' => $data]);
    }

    public function addnilai(Request $request, Siswa $siswa)
    {
        if ($siswa->mapel()->where('mapel_id', $request->mapel)->exists()) {
            return redirect('siswa/' . $siswa . '/profile')->with('error', 'Nilai mata pelajaran sudah ada');
        }
        $siswa->mapel()->attach($request->mapel, ['nilai' => $request->nilai]);

        return redirect('siswa/' . $siswa->id . '/profile')->with('sukses', 'Nilai berhasil dimasukkan');
    }

    public function deletenilai(Siswa $siswa, $idmapel)
    {
        $siswa->mapel()->detach($idmapel);
        return redirect()->back()->with('sukses', 'Data Nilai berhasil dihapus!');
    }

    public function exportExcel()
    {
        return Excel::download(new SiswaExport, 'siswa.xlsx');
    }

    public function exportPdf()
    {
        $siswa = Siswa::all();
        $pdf = PDF::loadView('export.siswaPdf', ['siswa' => $siswa]);
//        $pdf = PDF::loadHTML('<h1>LIST DATA SISWA</h1>');
        return $pdf->download('siswa.pdf');
    }

    public function getdatasiswa()
    {
        $siswa = Siswa::select('siswa.*');
        return DataTables::eloquent($siswa)
            ->addColumn('nama_lengkap', function ($s) {
                return $s->nama_depan . ' ' . $s->nama_belakang;
            })
            ->addColumn('nilaiRata2', function ($s) {
                return $s->nilaiRata2();
            })
            ->addColumn('aksi', function ($s) {
                return '<a href="/siswa/' . $s->id . '/profile/" class="btn btn-warning">Edit</a>';
            })
            ->addColumn('', function ($s) {
                return '<a href="#" class="btn btn-danger">Delete</a>';
            })
            ->rawColumns(['nama_lengkap', 'nilaiRata2', 'aksi', ''])
            ->toJson();
    }

    public function profilsaya()
    {
        $siswa = auth()->user()->siswa;
        return view('siswa.profilsaya', compact(['siswa']));
    }

    public function importexcel(Request $request)
    {
        Excel::import(new SiswaImport, $request->file('data_siswa'));
        $siswa = Siswa::all();
        return view('siswa.index', compact(['siswa']));
    }


}
