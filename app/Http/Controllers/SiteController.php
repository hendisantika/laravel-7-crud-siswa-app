<?php

namespace App\Http\Controllers;


use App\Mail\NotifPendaftaranSiswa;
use App\Post;
use App\Siswa;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SiteController extends Controller
{
    public function home()
    {
        $posts = Post::all();
        return view('sites.home', compact(['posts']));
    }

    public function about()
    {
        return view('sites.about');
    }

    public function register()
    {
        return view('sites.register');
    }

    public function daftar(Request $request)
    {
        //insert ke tabel user
        $user = new User;
        $user->role = 'siswa';
        $user->name = $request->nama_depan;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->remember_token = Str::random(60);
        $user->save();

        //insert ke tabel siswa
        $request->request->add(['user_id' => $user->id]);
        $siswa = Siswa::create($request->all());

//        Mail::raw('Halo ' . $user->name, function ($message) use ($user) {
//            $message->to($user->email, $user->name);
//            $message->subject('Pendaftaran Siswa');
//        });

        Mail::to($user->email)->send(new NotifPendaftaranSiswa());
        return redirect('/')->with('sukses', 'Data Pendaftaran berhasil dikirim');
    }

    public function singlepost($slug)
    {
        $post = Post::where('slug', '=', $slug)->first();
        return view('sites.singlepost', compact(['post']));
    }
}
