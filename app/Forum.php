<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class Forum extends Model
{
    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Model $model) {
            $model->setAttribute($model->getKeyName(), Uuid::uuid4()->getHex());
        });
    }

    protected $table = 'forum';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function forum()
    {
        return $this->hasMany(Komentar::class);
    }

}
