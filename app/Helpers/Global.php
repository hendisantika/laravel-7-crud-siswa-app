<?php

use App\Guru;
use App\Siswa;

function rangking5Besar()
{
    $siswa = Siswa::all();
    $siswa->map(function ($s) {
        $s->nilaiRata2 = $s->nilaiRata2();
        return $s;
    });
    $siswa = $siswa->sortByDesc('nilaiRata2')->take(5);
    return $siswa;
}

function totalSiswa()
{
    return Siswa::count();
}

function totalGuru()
{
    return Guru::count();
}
