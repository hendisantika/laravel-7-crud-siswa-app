<?php

namespace App\Exports;

use App\Siswa;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SiswaExport implements FromCollection, WithMapping, WithHeadings
{
    /**
     * @return Collection
     */
    public function collection()
    {
        return Siswa::all();
    }

    /**
     * @var Invoice $siswa
     */
    public function map($siswa): array
    {
        return [
            $siswa->namaLengkap(),
            $siswa->jenis_kelamin,
            $siswa->agama,
            $siswa->nilaiRata2()
        ];
    }

    public function headings(): array
    {
        return [
            'Nama Lengkap',
            'Jenis Kelamin',
            'Agama',
            'Nilai Rata2',
        ];
    }
}
