<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Siswa;
use Faker\Generator as Faker;

$factory->define(Siswa::class, function (Faker $faker) {
    return [
        'user_id' => '3a3ad0909e8611ea8254c4b301bd677f',
        'nama_depan' => $faker->firstName,
        'nama_belakang' => $faker->lastName,
        'jenis_kelamin' => $faker->randomElement(['L', 'P']),
        'agama' => $faker->randomElement(['Islam', 'Kristen', 'Protestan', 'Hindu', 'Budha', 'Konghucu']),
        'alamat' => $faker->address,
    ];
});
